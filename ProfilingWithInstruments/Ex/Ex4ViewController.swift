//
//  Ex4ViewController.swift
//  ProfilingWithInstruments
//
//  Created by Robert Musiałek on 12/07/16.
//  Copyright © 2016 Robert Musiałek. All rights reserved.
//

import UIKit

class Ex4Cell: UITableViewCell {
    
    var hue: CGFloat = 0.0
    var callback: (() -> Void)?
    
    @IBAction func buttonPressed(sender: AnyObject) {
        hue = 1.0 - hue
        backgroundColor = UIColor(hue: hue, saturation: 0.84, brightness: 0.95, alpha: 1.0)
        callback?()
    }
}

class Ex4ViewController: UITableViewController {
    let cellCount = 30
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellCount
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCellWithIdentifier("example-cell") as? Ex4Cell else {
            return UITableViewCell()
        }
        
        let hue = CGFloat(indexPath.row) / CGFloat(cellCount)
        cell.hue = hue
        cell.backgroundColor = UIColor(hue: hue, saturation: 0.84, brightness: 0.95, alpha: 1.0)
        
        cell.callback = { [weak self] _ in
            let message = "Tapped button at row: \(indexPath.row)"
            let alertController = UIAlertController(title: "Awesome!", message: message, preferredStyle: .Alert)
            alertController.addAction(UIAlertAction(title: "Ok!", style: .Default, handler: nil))
            self?.presentViewController(alertController, animated: true, completion: nil)
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
}
