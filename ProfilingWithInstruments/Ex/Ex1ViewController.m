//
//  Ex1ViewController.m
//  ProfilingWithInstruments
//
//  Created by Robert Musiałek on 11/07/16.
//  Copyright © 2016 Robert Musiałek. All rights reserved.
//

#import "Ex1ViewController.h"

@protocol Ex1Protocol <NSObject>

@optional
- (void)ex1ProtocolMethod;

@end

@interface Ex1ViewController () <Ex1Protocol>

@property (nonatomic, strong) NSArray<NSString *> *items;
@property (weak, nonatomic) IBOutlet UILabel *label;

@end

@implementation Ex1ViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.items = @[@"1",@"2",@"3"];
}

- (IBAction)buttonPressed:(id)sender {
    self.label.text = [NSString stringWithFormat:@"Last item is: %@", self.items[self.items.count - 1]];
}

- (IBAction)button2Pressed:(id)sender {
    [self performSelector:@selector(ex1ProtocolMethod)];
}

- (void)ex1ProtocolMethod {
    self.label.text = [NSString stringWithFormat:@"Set from %s",__PRETTY_FUNCTION__];
}

@end
