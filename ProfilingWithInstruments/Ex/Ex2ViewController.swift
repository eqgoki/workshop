//
//  Ex2ViewController.swift
//  ProfilingWithInstruments
//
//  Created by Robert Musiałek on 12/07/16.
//  Copyright © 2016 Robert Musiałek. All rights reserved.
//

import UIKit

class Ex2Cell: UITableViewCell {
    
    @IBOutlet weak var customImageView: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        customImageView.image = nil
    }
}

class Ex2ViewController: UITableViewController {
    let cellCount = 30
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellCount
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCellWithIdentifier("example-cell") as? Ex2Cell else {
            return UITableViewCell()
        }
        
        let hue = CGFloat(indexPath.row) / CGFloat(cellCount)
        cell.backgroundColor = UIColor(hue: hue, saturation: 0.84, brightness: 0.95, alpha: 1.0)
        var image = UIImage(named: "forest")
        if indexPath.row%10 == 9 {
            image = image?.applyTonalFilter()
        }
        cell.customImageView.image = image
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 200
    }
    
}

extension UIImage {
    func applyTonalFilter() -> UIImage? {
        let context = CIContext(options:nil)
        let filter = CIFilter(name:"CIPhotoEffectTonal")
        let input = CoreImage.CIImage(image: self)
        filter!.setValue(input, forKey: kCIInputImageKey)
        let outputImage = filter!.outputImage
        let outImage = context.createCGImage(outputImage!, fromRect: outputImage!.extent)
        let returnImage = UIImage(CGImage: outImage)
        return returnImage
    }
}