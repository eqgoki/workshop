//
//  Ex3ViewController.swift
//  ProfilingWithInstruments
//
//  Created by Robert Musiałek on 12/07/16.
//  Copyright © 2016 Robert Musiałek. All rights reserved.
//

import UIKit

class Ex3Cell: UITableViewCell {
    
    @IBOutlet weak var customImageView: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        customImageView.image = nil
    }
}

class Ex3ViewController: UITableViewController {
    let cellCount = 30
    var imageCache: Set<UIImage> = Set()
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellCount
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCellWithIdentifier("example-cell") as? Ex3Cell else {
            return UITableViewCell()
        }
        
        let hue = CGFloat(indexPath.row) / CGFloat(cellCount)
        cell.backgroundColor = UIColor(hue: hue, saturation: 0.84, brightness: 0.95, alpha: 1.0)
        let image = UIImage(named: "forest")
        imageCache.insert(image!)
        cell.customImageView.image = image
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 200
    }
    
}
