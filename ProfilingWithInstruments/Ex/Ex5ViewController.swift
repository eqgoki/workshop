//
//  Ex5ViewController.swift
//  ProfilingWithInstruments
//
//  Created by Robert Musiałek on 12/07/16.
//  Copyright © 2016 Robert Musiałek. All rights reserved.
//

import UIKit

class A {
    var b: B?
}

class B {
    var c: C?
}

class C {
    weak var a: A?
}

class Ex5ViewController: UIViewController {

    var a: A?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let a = A()
        let b = B()
        let c = C()
        
        a.b = b
        b.c = c
        c.a = a
        
        self.a = a
    }
}
