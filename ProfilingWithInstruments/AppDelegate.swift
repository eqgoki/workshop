//
//  AppDelegate.swift
//  ProfilingWithInstruments
//
//  Created by Robert Musiałek on 11/07/16.
//  Copyright © 2016 Robert Musiałek. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}

