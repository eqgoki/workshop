//
//  ViewController.swift
//  ProfilingWithInstruments
//
//  Created by Robert Musiałek on 11/07/16.
//  Copyright © 2016 Robert Musiałek. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let examples = ["Ex1", "Ex2", "Ex3", "Ex4", "Ex5"];
    @IBOutlet weak var tableView: UITableView!
}

extension ViewController : UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let segueId = examples[indexPath.row]
        performSegueWithIdentifier(segueId, sender: nil)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
}

extension ViewController : UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return examples.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCellWithIdentifier("example-cell") else {
            return UITableViewCell()
        }
        cell.textLabel?.text = examples[indexPath.row]
        return cell
    }
}